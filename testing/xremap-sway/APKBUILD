# Contributor: Coco Liliace <chloe@liliace.dev>
# Maintainer: Coco Liliace <chloe@liliace.dev>
pkgname=xremap-sway
pkgver=0.8.6
pkgrel=0
pkgdesc="Key remapper for X11 and Wayland"
url="https://github.com/k0kubun/xremap"
# nix
arch="all !s390x !riscv64"
license="MIT"
makedepends="cargo cargo-auditable"
source="$pkgname-$pkgver.tar.gz::https://github.com/k0kubun/xremap/archive/v$pkgver.tar.gz"
builddir="$srcdir/xremap-$pkgver"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release --features sway
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 -t "$pkgdir"/usr/bin ./target/release/xremap
}

sha512sums="
500c808fa7b892f5b9772b76408ce7b7fc046676ae26356b6559e47685b26da28f42ac0f2526606c6cfd64b5a2625fddc6d2bf20786294e2e423726d7da2eeb6  xremap-sway-0.8.6.tar.gz
"
