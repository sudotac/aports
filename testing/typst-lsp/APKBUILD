# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=typst-lsp
pkgver=0.7.2
pkgrel=2
pkgdesc="Language server for typst"
url="https://github.com/nvarner/typst-lsp"
# typst, rust-analyzer
arch="aarch64 ppc64le x86_64"
license="MIT"
depends="rust-analyzer"
makedepends="cargo cargo-auditable"
source="$pkgname-$pkgver.tar.gz::https://github.com/nvarner/typst-lsp/archive/refs/tags/v$pkgver.tar.gz"
options="net !check" # no tests

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

package() {
	install -Dm755 target/release/typst-lsp -t "$pkgdir"/usr/bin/
}

sha512sums="
dff6e8601b6a18f945cbee47ad7f7b3ea67f098c03ad74abad2f56ba789c306fb15a0a6c08173bb17712ed6c9cab1667540b93b2b897557edb96b95a0a27bec9  typst-lsp-0.7.2.tar.gz
"
