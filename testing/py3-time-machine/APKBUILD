# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=py3-time-machine
_pyname=${pkgname/py3-/}
pkgver=2.11.0
pkgrel=0
pkgdesc="Python library for mocking the current time"
url="https://github.com/adamchainz/time-machine"
arch="all"
license="MIT"
depends="python3 py3-dateutil"
makedepends="
	python3-dev
	py3-setuptools
	py3-gpep517
	py3-installer
	py3-wheel
	"
checkdepends="py3-pytest tzdata"
subpackages="$pkgname-pyc"
source="$_pyname-$pkgver.tar.gz::https://github.com/adamchainz/time-machine/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/${_pyname/-/_}-$pkgver-*.whl
}

sha512sums="
5bd5d76e2ecc392c249e86e2cfe54ee94a99a4e1cd1dd7e00b240e1299426d87a284ec270ae36aeb82acfe06d974d5aeca27ae977a480441e4daa5b852d00198  time-machine-2.11.0.tar.gz
"
