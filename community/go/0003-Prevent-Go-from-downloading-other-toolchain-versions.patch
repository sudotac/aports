From 3d87dbab87629c7f90253077d7833adc6e486cd3 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?S=C3=B6ren=20Tempel?= <soeren+git@soeren-tempel.net>
Date: Wed, 28 Jun 2023 15:01:04 +0200
Subject: [PATCH] Prevent Go from downloading other toolchain versions

Pre-built Go toolchain binaries are dynamically linked against glibc
and thus do not work on Alpine. Furthermore, we want to ensure that
users use the version of Go distributed with Alpine (i.e. including
our patches et cetera).

For this reason, we disable downloading of Go toolchains entirely.
We update the default go.env to reflect that by setting GOTOOLCHAIN
to 'path'.

See https://go.dev/doc/toolchain#select
---
 src/cmd/go/internal/toolchain/select.go | 7 ++++---
 1 file changed, 4 insertions(+), 3 deletions(-)

diff --git a/src/cmd/go/internal/toolchain/select.go b/src/cmd/go/internal/toolchain/select.go
index 8b1a0b94be..e9be0d720d 100644
--- a/src/cmd/go/internal/toolchain/select.go
+++ b/src/cmd/go/internal/toolchain/select.go
@@ -256,8 +256,9 @@ func Exec(gotoolchain string) {
 	}
 	os.Setenv(countEnv, fmt.Sprint(count+1))
 
-	env := cfg.Getenv("GOTOOLCHAIN")
-	pathOnly := env == "path" || strings.HasSuffix(env, "+path")
+	// On Alpine, we don't allow downloading other toolchain since
+	// these are linked dynamically against glibc (i.e. do not work).
+	pathOnly := true
 
 	// For testing, if TESTGO_VERSION is already in use
 	// (only happens in the cmd/go test binary)
@@ -290,7 +291,7 @@ func Exec(gotoolchain string) {
 	// GOTOOLCHAIN=auto looks in PATH and then falls back to download.
 	// GOTOOLCHAIN=path only looks in PATH.
 	if pathOnly {
-		base.Fatalf("cannot find %q in PATH", gotoolchain)
+		base.Fatalf("cannot find %q in PATH and toolchain downloading not supported", gotoolchain)
 	}
 
 	// Set up modules without an explicit go.mod, to download distribution.
