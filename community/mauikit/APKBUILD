# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=maui
pkgname=mauikit
pkgver=2.2.1
pkgrel=1
pkgdesc="Kit for developing MAUI Apps"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://mauikit.org/"
license="GPL-2.0-or-later"
depends="kirigami2"
depends_dev="
	$pkgname=$pkgver-r$pkgrel
	kconfig-dev
	kconfigwidgets-dev
	kdeclarative-dev
	kdecoration-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	kservice-dev
	mauiman-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	syntax-highlighting-dev
	xcb-util-wm-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/maui/mauikit/$pkgver/mauikit-$pkgver.tar.xz"
subpackages="$pkgname-dev"

_commit=""
_repo_url="https://invent.kde.org/maui/mauikit.git"
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 $_repo_url .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
852e69deb633cc7c64a429d782a977f9e1fa26b797591740c22cddc7dc250673389e45d19704bad56332587eefe4da5832f40e4e2fccf1ce244ceb4d6ef0f265  mauikit-2.2.1.tar.xz
"
