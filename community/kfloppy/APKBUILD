# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kfloppy
pkgver=23.04.3
pkgrel=1
pkgdesc="A utility that provides a straightforward graphical means to format 3.5\" and 5.25\" floppy disks"
url="https://kde.org/applications/utilities/org.kde.kfloppy"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-or-later"
depends="
	coreutils
	dosfstools
	e2fsprogs
	"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/kfloppy.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kfloppy-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
550784b7c08ff71e6154f8d14b4d16d2987b7a32d0c27b185c8ede7d077b3125bc9c6f9a969af074f1f16b0f41e7bc4cfc667a7be93861d78e3e759dec1f8424  kfloppy-23.04.3.tar.xz
"
