# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kruler
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/graphics/org.kde.kruler"
pkgdesc="An on-screen ruler for measuring pixels"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdoctools-dev
	ki18n-dev
	knotifications-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	"
_repo_url="https://invent.kde.org/graphics/kruler.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kruler-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
165ae839a26a094068e59260dddeb9614b2dfe0b5933044fc93fc3fab423d1ad0f278cd1947b14a3fd6004acaf18bc071fff7ae693c7d2e3f1683080e8524b23  kruler-23.04.3.tar.xz
"
