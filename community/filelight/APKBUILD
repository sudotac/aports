# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=filelight
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/utilities/filelight"
pkgdesc="An application to visualize the disk usage on your computer"
license="(GPL-2.0-only OR GPL-3.0-only) AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/filelight.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/filelight-$pkgver.tar.xz
	0001-Fix-musl-build.patch
	"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ff1661c986a2c5d061ee6cfab41c99e06438b21562456de82dbc7fe72f717f20d883e4e9cd8db7d4f6e41aa5f81c6f27b50658db3d5b14b19b6f66c86ba0407a  filelight-23.04.3.tar.xz
db0d2fe06a3073c981814989edbaa97cd8b14fa05894c735273b30fc3f95c69207db22bd87db31a703d8f187d1b65827663d60192ba979b977ad51d4cdaa37ad  0001-Fix-musl-build.patch
"
