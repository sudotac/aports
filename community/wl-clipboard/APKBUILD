# Contributor: Robert Sacks <robert@sacks.email>
# Maintainer: Robert Sacks <robert@sacks.email>
pkgname=wl-clipboard
pkgver=2.2.0
pkgrel=0
pkgdesc="Command-line copy/paste utilities for Wayland"
url="https://github.com/bugaevc/wl-clipboard"
arch="all"
license="GPL-3.0-or-later"
options="!check" # No test suite
makedepends="
	meson
	ninja
	wayland-dev
	wayland-protocols
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="https://github.com/bugaevc/wl-clipboard/archive/v$pkgver/wl-clipboard-$pkgver.tar.gz"

build() {
	abuild-meson -Db_lto=true . build
	meson compile -C build
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C build

	# Install fish completion files into the correct directory
	rm -r "$pkgdir"/usr/share/fish/vendor_completions.d
	install -Dm644 completions/fish/wl-copy.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/wl-copy.fish
	install -Dm644 completions/fish/wl-paste.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/wl-paste.fish
}

sha512sums="
a2667b26970ebdd4c6d8bb6a05d902fa446fb94ba2c878dfa896e502277dac9837c75370108de9a39308597c153f3952289933174adf535148d027593a6cf829  wl-clipboard-2.2.0.tar.gz
"
