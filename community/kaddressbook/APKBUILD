# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kaddressbook
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://apps.kde.org/kaddressbook/"
pkgdesc="Address Book application to manage your contacts"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends="kdepim-runtime"
makedepends="
	akonadi-dev
	akonadi-search-dev
	extra-cmake-modules
	gpgme-dev
	grantleetheme-dev
	kcmutils-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kontactinterface-dev
	kpimtextedit-dev
	kuserfeedback-dev
	kuserfeedback-dev
	libkdepim-dev
	libkleo-dev
	pimcommon-dev
	prison-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/pim/kaddressbook.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaddressbook-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja\
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a072b98eb440cc262a318e8fa3030e555b10c44264512cef6dbdb0f06333878e3ff293eab01d0bbbb5cc8ed3ddbc007b87ec74f5d68af7ee64eabba61626520c  kaddressbook-23.04.3.tar.xz
"
