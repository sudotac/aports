# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=purpose
pkgver=5.108.0
pkgrel=2
pkgdesc="Framework for providing abstractions to get the developer's purposes fulfilled"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> kaccounts-integration
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kaccounts-integration-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
options="!check" # fails on builders
_repo_url="https://invent.kde.org/frameworks/purpose.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/purpose-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/purpose.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure -E 'menutest'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4dd031ea685c54d06b06ad8ff24d6b7ddd68e5bf27391817889dd0cb377ebdbb8ea6e030bf215e9c93e29b61e7bd62e17ad576e6f8a4189278f6b99e47f02409  purpose-5.108.0.tar.xz
"
